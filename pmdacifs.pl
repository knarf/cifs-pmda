#
# Copyright (c) 2013 Aurélien Aptel <aurelien.aptel@gmail.com>
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
#

use strict;
use warnings;
use PCP::PMDA;
use Data::Dumper;

my $dom = 'cifs';
my $pmid = 200; # TODO: ask for pmid upstream in pcp mailing list
my $procfile = '/proc/fs/cifs/Stats';
my %stat;
my $pmda;
my $mount_items_indom;
my @root_items;
my @mount_items;
my @mount_paths;
my @total_items;
my $nb_mount;
my $nb_item;
my @indom_desc;

main();

sub main {
    parse_stat_file();

    @root_items  = qw/numsession numoperation/;
    @mount_items = keys %{$stat{permount}[0]{data}};
    @mount_paths = map {$_->{path}} @{$stat{permount}};
    @total_items = map {"total.$_"} @mount_items;
    $nb_mount = @{$stat{permount}};
    $nb_item = @mount_items;
    my $byte_unit = pmda_units(1,0,0, PM_SPACE_BYTE,0,0);
    my $count_unit = pmda_units(0,0,0,0,0,0);
    my @item = (0, 0, 0);

    $pmda = PCP::PMDA->new($dom, $pmid);
    $pmda->log("added pmda id $pmid");

    # cluster 0 for global OS stats
    # cluster 1 for permount stat
    # cluster 2 for sum of mount stat (total)
    for (@root_items, @total_items) {
        my $path = "$dom.$_";
        my $unit = /bytes/i ? $byte_unit : $count_unit;
        my $cluster = (/total\./ ? 1 : 0);
        my $pmid = pmda_pmid($cluster, $item[$cluster]);

        $pmda->add_metric($pmid, PM_TYPE_U32, PM_INDOM_NULL,
                          PM_SEM_COUNTER, $unit, $path, '', '');

        $pmda->log("add_metric ($cluster, $item[$cluster]) $pmid, $path");

        $item[$cluster]++;
    }

    for (@mount_items) {
        my $path = "$dom.$_";
        my $unit = /bytes/i ? $byte_unit : $count_unit;
        my $cluster = 2;
        my $pmid = pmda_pmid($cluster, $item[$cluster]);
        my $indom = $item[$cluster];

        $pmda->add_metric($pmid, PM_TYPE_U32, $indom,
                          PM_SEM_COUNTER, $unit, $path, '', '');

        $pmda->log("add_metric ($cluster, $item[$cluster]) $pmid, $path INDOM $indom");

        $item[$cluster]++;
    }

    # build description of each instance (mount)
    # use share name
    for my $i (0 .. $nb_mount-1) {
        $indom_desc[$i*2] = $i;
        $indom_desc[$i*2+1] = $mount_paths[$i];
    }

    # $pmda->log("built desc ". Dumper(\@indom_desc));

    # create indom for each per mount stat and pass a copy of the
    # description array to describe each instance
    for my $indom (0 .. $nb_item-1) {
        $pmda->add_indom($indom, [@indom_desc], '', '');
        # $pmda->log("add_indom $indom");
    }

    $pmda->set_fetch(\&cifs_fetch);
    $pmda->set_fetch_callback(\&cifs_fetch_callback);
    $pmda->set_user('pcp');
    $pmda->run;
}

sub parse_stat_file {
    open my $h, '<', $procfile;

    my $inmount = 0;
    my $id = -1;
    while (<$h>) {
        chomp;

        if (/^(\d+)\) (.+)/) {
            $id = int($1) - 1;
            $stat{permount}[$id]{path} = $2;
            $inmount = 1;
            next;
        }

        if (!$inmount) {
            if (/^CIFS Session: (\d+)/) {
                $stat{numsession} = int($1);
            }
            if (/^Operations.+?: (\d+)/) {
                $stat{numoperation} = int($1);
            }
        } else {
            my $n = 0;
            my $root = '';
            while (/\s*(.+?):?\s+(\d+)/g) {
                my ($k, $v) = (fix_name($1), int($2));
                if ($n == 0) {
                    $root = $k;
                } elsif ($k eq 'Bytes') {
                    # multiple 'Bytes' fields: prepend root key for
                    # read/write bytes field name to prevent
                    # overwriting
                    $k = $root.$k;
                }

                $stat{permount}[$id]{data}{$k} = $v;
                $stat{total}{$k} += $v;
                $n++;
            }
        }
    }
}

sub fix_name {
    my $s = shift;
    $s =~ s/[^a-zA-Z0-9]/_/g;
    return $s;
}

sub cifs_fetch {
    parse_stat_file();
    # $pmda->log("fetch: done");
}

sub cifs_fetch_callback {
    my ($cluster, $item, $inst) = @_;

    # $pmda->log("fetch_cb: $cluster, $item, $inst");

    if ($cluster == 0) {
        if ($inst != PM_IN_NULL) {
            return (PM_ERR_INST, 0);
        }
        return ($stat{$root_items[$item]}, 1);
    }
    elsif ($cluster == 1) {
        if ($inst != PM_IN_NULL) {
            return (PM_ERR_INST, 0);
        }
        return ($stat{total}{$mount_items[$item]}, 1);
    }
    elsif ($cluster == 2) {
        if ($inst == PM_IN_NULL) {
            return (PM_ERR_INST, 0);
        }
        return ($stat{permount}[$inst]{data}{$mount_items[$item]}, 1);
    }

    $pmda->log("fetch_cb: not supposed to be here...");
    return (PM_ERR_PMID, 0);
}
