
NAME    = cifs
PMDADIR = /var/lib/pcp/pmdas
DIR     = $(PMDADIR)/$(NAME)
SCRIPT  = pmdacifs.pl
SRC     = Install Remove $(SCRIPT)

install: check
	rm -rf $(DIR)
	mkdir $(DIR)
	cp $(SRC) $(DIR)
	cd $(DIR) && echo c | ./Install

check: $(SCRIPT)
	@echo checking if $(PMDADIR) exists...
	[ -d $(PMDADIR) ]
	@echo checking script syntax...
	perl -c $^

.PHONY: install check
